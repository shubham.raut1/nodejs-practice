const mongoose = require("mongoose");

const DB = "mongodb+srv://sr:admin@cluster0.byxqez0.mongodb.net/db?retryWrites=true&w=majority";

module.exports = () => {
    try {
        mongoose.connect(DB);
        mongoose.connection.once("open", () => {
            console.log("Connection Success");
        })
        mongoose.connection.on("error", (err) => {
            console.log("Connection err ", err);
        })
    } catch (err) {
        console.log(err)
    }
};