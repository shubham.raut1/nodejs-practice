const SuperAdmin = require("../models/superadmin.js");
const UserModel = require("../models/user.js");
const jwt = require("jsonwebtoken");

exports.verifyUser = async (req, res, next) => {
    try {
        if(req.headers && req.headers.authorization && req.headers.authorization.split(" ")[0] === "JWT") {
            const token = req.headers.authorization.split(" ")[1];
            const decode = jwt.verify(token, "shubham");
            const user = await UserModel.findOne({ _id: decode.id });
            if(user) {
                req.isAuthorized = true;
            } else {
                req.isAuthorized = false;
            }
        } else {
            req.isAuthorized = false;
        }
        next();
    } catch (err) {
        res.status(500).send(err);
    }
}

exports.verifySuperAdmin = async (req, res, next) => {
    try {
        const user = await SuperAdmin.findOne({email: req.body.email});
        if(user) {
            next();
        } else {
            res.send("Login as a admin");
        }
    } catch (err) {
        res.send({err})
    }
}
