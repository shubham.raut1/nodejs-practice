const User = require("../models/user.js")

exports.getDeatils = async (req, res) => {
    try {
        let user = await User.findById(req.params.id);
        res.send(user);
    } catch (err) {
        res.send(err);
    }
}

exports.updateUser = async (req, res) => {
    try {
        let user = await User.findByIdAndUpdate(req.params.id, req.body);
        await user.save();
        res.send(user);
    } catch (err) {
        res.send(err);
    }
}

exports.createUser = async (req, res) => {
    try {
        let user = new User(req.body);
        await user.save();
        res.send(user);
    } catch (err) {
        res.send(err);
    }
}

exports.deleteUser = async (req, res) => {
    try {
        let user = await User.findByIdAndDelete(req.params.id);
        await user.save();
        res.send(user);
    } catch (err) {
        res.send(err);
    }
}