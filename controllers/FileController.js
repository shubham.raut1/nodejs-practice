const fs = require("fs");
const path = require("path");
const root = path.join(__dirname, "..")

exports.createFile = (req, res) => {
    try {
        if(!fs.existsSync(`${root}/files`)) {
            fs.mkdir(`${root}/files`, (err) => {
                if(err) {
                    res.send("Error While Creating File");
                    throw err;
                }
                fs.writeFile(`${root}/files/${req.body.filename}`, `${req.body.filecontent}`);
                res.send("File Created Successfully");
            });
        } else {
            fs.writeFile(`${root}/files/${req.body.filename}`, `${req.body.filecontent}`, (err) => {
                if(err) {
                    res.send("Error While Creating File");
                    throw err;
                }
                res.send("File Created Successfully");
            });
        }
    } catch (err) {
        console.log(err);
    }
}

exports.readFile = (req, res) => {
    try {
        fs.readFile(`${root}/files/${req.body.filename}`, (err, data) => {
            if(err) {
                res.send("Error While Reading File");
            }
            res.send(`Your Data: ${data}`);
        });
    } catch (err) {
        res.send("Error While Reading File");
    }
}

exports.updateFile = (req, res) => {
    try {
        fs.writeFile(`${root}/files/${req.body.filename}`, `${req.body.filecontent}`, (err) => {
            if(err) {
                res.send("File Already Exixts");
                throw err;
            }
            res.send("File Updated Successfully");
        });
    } catch (err) {
        res.send("Error While Updating File");
    }
}

exports.deleteFile = (req, res) => {
    try {
        fs.unlink(`${root}/files/${req.body.filename}`, (err) => {
            if(err) throw err;
            res.send("File Deleted");
        });
    } catch (err) {
        res.send("Error While Deleting The File");
    }
}