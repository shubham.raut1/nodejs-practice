const mongoose = require("mongoose");

const SuperAdmin = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
});

const SuperAdminModel = mongoose.model("superadmin", SuperAdmin);

module.exports = SuperAdminModel;
