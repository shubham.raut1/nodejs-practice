const mongoose = require("mongoose");

const SuperUser = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
});

const SuperUserModel = mongoose.model("superuser", SuperUser);

module.exports = SuperUserModel;
