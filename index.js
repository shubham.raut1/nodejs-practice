const express = require("express");
const path = require("path");
// const AdminRouter = require("./routes/admin.js");
// const UserRouter = require("./routes/user.js");
// const FileRouter = require("./routes/file.js");
const SuperAdmin = require("./routes/superadmin.js");
const SuperUser = require("./routes/superuser.js");
const dbConnection = require("./config/db.js");
const app = express();
const PORT = 8502;

app.use(express.json());
app.use(express.static("public"));
dbConnection();

// app.use("/admin", AdminRouter);
// app.use("/user", UserRouter);
// app.use("/file", FileRouter);

app.use("/superadmin", SuperAdmin);
app.use("/superuser", SuperUser);

app.listen(PORT, () => {
    console.log(`Server Is Running at locahost ${PORT}`);
});