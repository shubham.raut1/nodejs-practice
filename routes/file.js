const express = require("express");
const router = express.Router();
const file = require("../controllers/FileController.js")

router.post("/create-file", file.createFile);

router.post("/read-file", file.readFile);

router.post("/update-file", file.updateFile);

router.post("/delete-file", file.deleteFile);

module.exports = router;
