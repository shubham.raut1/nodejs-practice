const express = require("express");
const { verifySuperAdmin } = require("../middlewares/auth");
const SuperUser = require("../models/superuser");
const router = express.Router();

router.post("/create-user", async (req, res) => {
    try {
        const user = SuperUser(req.body);
        await user.save();
        res.send(user);
    } catch (err) {
        res.send({err});
    }
});

router.get("/get-users", verifySuperAdmin, async (req, res) => {
    try {
        const user = await SuperUser.find();
        if(user) {
            res.send(user);
        } else {
            res.send("User List Is Empty");
        }
    } catch (err) {
        res.send("Error While Fetching The Users");
    }
});

module.exports = router;
