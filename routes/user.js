const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController.js");

router.get("/get-deatils/:id", UserController.getDeatils);

router.post("/update-user/:id", UserController.updateUser);

router.post("/create-user", UserController.createUser);

router.post("/delete-user/:id", UserController.deleteUser);

module.exports = router;