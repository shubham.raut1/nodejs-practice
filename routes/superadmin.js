const express = require("express");
const SuperAdmin = require("../models/superadmin");
const { verifySuperAdmin } = require("../middlewares/auth");
const router = express.Router();

router.post("/create-user", async (req, res) => {
    try {
        const user = SuperAdmin(req.body);
        await user.save();
        res.send(user);
    } catch (err) {
        res.send({err});
    }
});

router.get("/get-users", verifySuperAdmin, async (req, res) => {
    try {
        const user = await SuperAdmin.find();
        if(user) {
            res.send(user);
        } else {
            res.send("User List Is Empty");
        }
    } catch (err) {
        res.send("Error While Fetching The Users");
    }
});

module.exports = router;
