const express = require("express");
const router = express.Router();
const AdminController = require("../controllers/AdminController.js");
const { verifyUser } = require("../middlewares/auth.js");
const UserModel = require("../models/user.js");
const jwt = require("jsonwebtoken");

router.get("/sign-in", async (req, res) => {
    const user = await UserModel.findOne({email: req.body.email});
    if(user) {
        const token = jwt.sign({
            id: user.id,
        }, "shubham", { expiresIn: 86400 });
        res.send({
            name: user.name,
            email: user.email,
            accessToken: token
        });
    } else {
        res.send("Please Sign up");
    }
});

router.get("/get-users", verifyUser, AdminController.getUser);

router.post("/update-user/:id", AdminController.updateUser);

module.exports = router;